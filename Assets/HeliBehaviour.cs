﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeliBehaviour : MonoBehaviour {
	Rigidbody rb;
	public float upVelocity;
	public float downVelocity;
	public float rotateSpeed;
	public float moveSpeed;
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {

		//HACER EN FIXED CON BOOLEANOS
		if(Input.GetKey(KeyCode.LeftControl)){
			rb.AddRelativeForce (Vector3.up*upVelocity, ForceMode.Acceleration);
		}
		if(Input.GetKey(KeyCode.LeftShift)){
			rb.AddRelativeForce (Vector3.down*downVelocity, ForceMode.Acceleration);
		}


		if(Input.GetKey(KeyCode.W)){
			rb.AddRelativeForce(Vector3.back*moveSpeed, ForceMode.Acceleration);
		}
		if(Input.GetKey(KeyCode.S)){
			rb.AddRelativeForce (Vector3.forward*moveSpeed, ForceMode.Acceleration);
		}
		if(Input.GetKey(KeyCode.A)){
			rb.AddRelativeForce (Vector3.right*moveSpeed, ForceMode.Acceleration);
		}
		if(Input.GetKey(KeyCode.D)){
			rb.AddRelativeForce (Vector3.left*moveSpeed, ForceMode.Acceleration);
		}


		if(Input.GetKey(KeyCode.E)){
			transform.Rotate (Vector3.up * Time.deltaTime * rotateSpeed);
		}
		if(Input.GetKey(KeyCode.Q)){
			transform.Rotate (Vector3.down * Time.deltaTime * rotateSpeed);
		}
	}
}
